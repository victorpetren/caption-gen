This project is an modification/extension of the "Grounded Translation" GitHub project (https://github.com/elliottd/GroundedTranslation) originally by Desmond Elliott.

The model implemented in this project jointly learns to generate descriptions for images in multiple languages. See model illustration below:

![](src/model.png)

Consult the report for additional details.
