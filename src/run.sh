#!/bin/bash

THEANO_FLAGS=floatX=float32,device=gpu2 python main.py --dataset iaprtc12_eng --dataset_mtl iaprtc12_ger --hidden_size=256 --fixed_seed --run_string=fs_mlt_256 --meteor_lang1=en --meteor_lang2=de
