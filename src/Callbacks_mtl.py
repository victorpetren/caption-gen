"""
Module to do callbacks for Keras models.
"""
from __future__ import division

from keras.callbacks import Callback  # ModelCheckpoint , EarlyStopping

import h5py
import itertools
import logging
import numpy as np
import os
import subprocess
import shutil
import codecs
import sys
from time import gmtime, strftime
import math
import time
from copy import deepcopy

# Set up logger
logging.basicConfig(level=logging.INFO, stream=sys.stdout)
#logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

# Dimensionality of image feature vector
IMG_FEATS = 4096
HSN_SIZE = 409
MULTEVAL_DIR = '../multeval-0.5.1' if "util" in os.getcwd() else "multeval-0.5.1"


class cd:
    """Context manager for changing the current working directory"""
    """http://stackoverflow.com/questions/431684/how-do-i-cd-in-python"""
    def __init__(self, newPath):
        self.newPath = newPath

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

class CompilationOfCallbacksMTL(Callback):
    """ Collection of compiled callbacks."""

    def __init__(self, word2index_d1, word2index_d2,
                       index2word_d1, index2word_d2,
                       argsDict,
                       dataset1, dataset2,
                       data_generator,
                       use_sourcelang=False, use_image=True, halt_scoring=False):
        super(Callback, self).__init__()

        self.verbose = True
        self.filename = "weights.hdf5"
        self.save_best_only = True

        self.val_loss1, self.val_loss2 = [], []
        self.best_val_loss1, self.best_val_loss2 = np.inf, np.inf

        self.val_metric1, self.val_metric2 = [], []
        self.best_val_metric1, self.best_val_metric2 = np.NINF, np.NINF
        self.val_bl_metric1, self.val_bl_metric2 = [], []
        self.best_val_bl_metric1, self.best_val_bl_metric2 = np.NINF, np.NINF


        self.word2index_d1 = word2index_d1
        self.word2index_d2 = word2index_d2
        self.index2word_d1 = index2word_d1
        self.index2word_d2 = index2word_d2
        self.args = argsDict

        # used to control early stopping on the validation data
        self.wait = 0
        self.patience = self.args.patience

        # needed by model.predict in generate_sentences
        self.use_sourcelang = use_sourcelang
        self.use_image = use_image

        # controversial assignment but it makes it much easier to
        # do early stopping based on metrics
        self.data_generator = data_generator

        # this results in two file handlers for dataset (here and
        # data_generator)
        if not dataset1 or not dataset2:
            logger.warn("No dataset given, using flickr8k")
            self.dataset1 = h5py.File("flickr8k/dataset.h5", "r")
            self.dataset2 = h5py.File("flickr8k/dataset.h5", "r")
        else:
            self.dataset1 = h5py.File("%s/dataset.h5" % dataset1, "r")
            self.dataset2 = h5py.File("%s/dataset.h5" % dataset2, "r")
        if self.args.source_vectors is not None:
            self.source_dataset = h5py.File("%s/dataset.h5" % self.args.source_vectors, "r")

        self.d1, self.d2 = dataset1, dataset2
        self.halt_scoring = halt_scoring

    def on_epoch_end(self, epoch, logs={}):
        '''
        At the end of each epoch we
          1. create a directory to checkpoint data
          2. save the arguments used to initialise the run
          3. generate N sentences in the val data by sampling from the model
          4. calculate metric score of the generated sentences
          5. determine whether to stop training and sys.exit(0)
          6. save the model parameters using BLEU
        '''
        savetime = strftime("%d%m%Y-%H%M%S", gmtime())
        path = self.create_checkpoint_directory(savetime)
        self.save_run_arguments(path)

        # Generate training and val sentences to check for overfitting
        if not self.halt_scoring:
            self.generate_sentences(path)
            meteor1, bleu1, ter1, meteor2, bleu2, ter2 = self.multeval_scores(path)
            val_loss = logs.get('val_loss')
            val_loss1, val_loss2 = logs.get('val_output1_loss'), logs.get('val_output2_loss')

            self.early_stop_decision(len(self.val_metric1)+1, meteor1, meteor2, val_loss1, val_loss2, bleu1, bleu2)
            self.checkpoint_parameters(epoch, logs, path, meteor1, meteor2, bleu1, bleu2, val_loss1, val_loss2)
            self.log_performance()
        else:
            #Only save weights to save time and perform metric scoring after training only
            weights_path = "%s/weights.hdf5" % path
            self.model.save_weights(weights_path, overwrite=True)

    def on_train_end(self, logs={}):
        savetime = strftime("%d%m%Y-%H%M%S", gmtime())
        path = self.create_checkpoint_directory(savetime, train_end = True)
        self.save_run_arguments(path)

        # Generate training and val sentences to check for overfitting
        self.generate_sentences(path)
        meteor1, bleu1, ter1, meteor2, bleu2, ter2 = self.multeval_scores(path)
        val_loss = logs.get('val_loss')
        val_loss1, val_loss2 = logs.get('val_output1_loss'), logs.get('val_output2_loss')

        self.early_stop_decision(len(self.val_metric1)+1, meteor1, meteor2, val_loss1, val_loss2, bleu1, bleu2)
        self.checkpoint_parameters('', logs, path, meteor1, meteor2, bleu1, bleu2, val_loss1, val_loss2)
        self.log_performance()

    def early_stop_decision(self, epoch, val_metric1, val_metric2, val_loss1, val_loss2, bleu1, bleu2):
        '''
	Stop training if validation loss has stopped decreasing and
	validation BLEU score has not increased for --patience epochs.

        WARNING: quits with sys.exit(0).

	TODO: this doesn't yet support early stopping based on TER
        '''
	if val_loss1 < self.best_val_loss1 or val_loss2 < self.best_val_loss2:
	    self.wait = 0
        elif val_metric1 > self.best_val_metric1 or val_metric2 > self.best_val_metric2 or self.args.no_early_stopping:
            self.wait = 0
        elif bleu1 > self.best_val_bl_metric1 or bleu2 > self.best_val_bl_metric2 or self.args.no_early_stopping:
            self.wait = 0
        else:
            self.wait += 1
            if self.wait >= self.patience:
                # we have exceeded patience
                if val_loss1 > self.best_val_loss1 or val_loss2 > self.best_val_loss2:
                    # and loss is no longer decreasing
                    logger.info("Epoch %d: early stopping", epoch)
                    handle = open("checkpoints/%s/summary"
                                  % self.args.run_string, "a")
                    handle.write("Early stopping because patience exceeded\n")
                    best_meteor1 = np.nanargmax(self.val_metric1)
                    best_meteor2 = np.nanargmax(self.val_metric2)
                    best_bleu1 = np.nanargmax(self.val_bl_metric1)
                    best_bleu2 = np.nanargmax(self.val_bl_metric2)
                    best_loss1 = np.nanargmin(self.val_loss1)
                    best_loss2 = np.nanargmin(self.val_loss2)
                    logger.info("Best %s METEOR Metric: %d | val loss %.5f score %.2f",
                                self.d1, best_meteor1+1, self.val_loss1[best_meteor1],
                                self.val_metric1[best_meteor1])
                    logger.info("Best %s METEOR Metric: %d | val loss %.5f score %.2f",
                                self.d2, best_meteor2+1, self.val_loss2[best_meteor2],
                                self.val_metric2[best_meteor2])
                    logger.info("Best %s BLEU Metric: %d | val loss %.5f score %.2f",
                                self.d1, best_bleu1+1, self.val_loss1[best_bleu1],
                                self.val_bl_metric1[best_bleu1])
                    logger.info("Best %s BLEU Metric: %d | val loss %.5f score %.2f",
                                self.d2, best_bleu2+1, self.val_loss2[best_bleu2],
                                self.val_bl_metric2[best_bleu2])


                    logger.info("Best %s loss: %d | val loss %.5f score %.2f",
                                self.d1, best_loss1+1, self.val_loss1[best_loss1],
                                self.val_metric1[best_loss1])
                    logger.info("Best %s loss: %d | val loss %.5f score %.2f",
                                self.d2, best_loss2+1, self.val_loss2[best_loss2],
                                self.val_metric2[best_loss2])

                    handle.close()
                    sys.exit(0)

    def log_performance(self):
        '''
        Record model performance so far, based on validation loss.
        '''
        handle = open("checkpoints/%s/summary" % self.args.run_string, "w")

        for epoch in range(len(self.val_loss1)):
            handle.write("Checkpoint %d for %s | val loss: %.5f meteor: %.2f bleu: %.2f\n"
                         % (epoch+1, self.d1, self.val_loss1[epoch],
                            self.val_metric1[epoch], self.val_bl_metric1[epoch]))
            handle.write("Checkpoint %d for %s | val loss: %.5f meteor: %.2f bleu: %.2f\n"
                         % (epoch+1, self.d2, self.val_loss2[epoch],
                            self.val_metric2[epoch], self.val_bl_metric2[epoch]))


        logger.info("---")  # break up the presentation for clarity

        # BLEU is the quickest indicator of performance for our task
        # but loss is our objective function
        best_meteor1 = np.nanargmax(self.val_metric1)
        best_meteor2 = np.nanargmax(self.val_metric2)
        best_bleu1 = np.nanargmax(self.val_bl_metric1)
        best_bleu2 = np.nanargmax(self.val_bl_metric2)
        best_loss1 = np.nanargmin(self.val_loss1)
        best_loss2 = np.nanargmin(self.val_loss2)

        best_metric_str1 = "Best %s METEOR/BLEU Metric: %d / %d | val loss: %.5f / %.5f score: %.2f / %.2f\n" % (self.d1, best_meteor1+1, best_bleu1+1, self.val_loss1[best_meteor1], self.val_loss1[best_bleu1], self.val_metric1[best_meteor1], self.val_bl_metric1[best_bleu1])
        best_metric_str2 = "Best %s METEOR/BLEU Metric: %d / %d | val loss: %.5f / %.5f score: %.2f / %.2f\n" % (self.d2, best_meteor2+1, best_bleu2+1, self.val_loss2[best_meteor2], self.val_loss2[best_bleu2], self.val_metric2[best_meteor2], self.val_bl_metric2[best_bleu2])
        #best_metric_str2 = "Best %s METEOR/BLEU Metric: %d / %d | val loss: %.5f / %.5f score: %.2f / %.2f\n" % (self.d2, best_meteor2+1, self.val_loss2[best_meteor2], self.val_metric2[best_meteor2])

        #best_bl_metric_str1 = "Best %s BLEU Metric: %d | val loss %.5f score %.2f\n" % (self.d1, best_bleu1+1, self.val_loss1[best_bleu1], self.val_bl_metric1[best_bleu1])
        #best_bl_metric_str2 = "Best %s BLEU Metric: %d | val loss %.5f score %.2f\n" % (self.d2, best_bleu2+1, self.val_loss2[best_bleu2], self.val_bl_metric2[best_bleu2])


        logger.info(best_metric_str1)
        logger.info(best_metric_str2)
        #logger.info(best_bl_metric_str1)
        #logger.info(best_bl_metric_str2)


        handle.write(best_metric_str1)
        handle.write(best_metric_str2)
        #handle.write(best_bl_metric_str1)
        #handle.write(best_bl_metric_str2)


        best_loss_str1 = "Best %s loss: %d | val loss: %.5f METEOR/BLEU score: %.2f / %.2f\n" % (self.d1, best_loss1+1, self.val_loss1[best_loss1], self.val_metric1[best_loss1], self.val_bl_metric1[best_loss1])
        best_loss_str2 = "Best %s loss: %d | val loss: %.5f METEOR/BLEU score: %.2f / %.2f\n" % (self.d2, best_loss2+1, self.val_loss2[best_loss2], self.val_metric2[best_loss2], self.val_bl_metric2[best_loss2])

        logger.info(best_loss_str1)
        logger.info(best_loss_str2)

        handle.write(best_loss_str1)
        handle.write(best_loss_str2)

        logger.info("Early stopping marker: wait/patience: %d/%d\n",
                    self.wait, self.patience)
        handle.write("Early stopping marker: wait/patience: %d/%d\n" %
                     (self.wait, self.patience))
        handle.close()

    def extract_references(self, directory, split):
        """
        Get reference descriptions for val or test data.
        """
        references1, references2 = self.data_generator.get_refs_by_split_as_list(split)

        for refid in xrange(len(references1[0])):
            codecs.open('%s/%s_%s_reference.ref%d' % (directory, self.d1, split, refid),
                        'w', 'utf-8').write('\n'.join([x[refid] for x in references1]))
                        #'w', 'utf-8').write('\n'.join(['\n'.join(x) for x in references]))
        for refid in xrange(len(references2[0])):
            codecs.open('%s/%s_%s_reference.ref%d' % (directory, self.d2, split, refid),
                        'w', 'utf-8').write('\n'.join([x[refid] for x in references2]))

        return references1, references2

    #def __bleu_score__(self, directory, val=True):
    #    '''
    #    Loss is only weakly correlated with improvements in BLEU,
    #    and thus improvements in human judgements. Let's also track
    #    BLEU score of a subset of generated sentences in the val split
    #    to decide on early stopping, etc.
    #    '''
    #
    #    prefix = "val" if val else "test"
    #
    #    self.extract_references(directory, split=prefix)
    #
    #    subprocess.check_call(
    #        ['perl multi-bleu.perl %s/%s_reference.ref < %s/%sGenerated > %s/%sBLEU'
    #         % (directory, prefix, directory, prefix, directory, prefix)],
    #        shell=True)
    #    bleudata = open("%s/%sBLEU" % (directory, prefix)).readline()
    #    data = bleudata.split(",")[0]
    #    bleuscore = data.split("=")[1]
    #    bleu = float(bleuscore.lstrip())
    #    return bleu

    def multeval_scores(self, directory, val=True):
        '''
        Maybe you want to do early stopping using Meteor, TER, or BLEU?
        '''
        prefix = "val" if val else "test"
        self.extract_references(directory, prefix)

        # First you want re-compound the split German words
        if self.args.meteor_lang1 == 'de' or self.args.meteor_lang2 == 'de':
            de = self.d1 if self.args.meteor_lang1 == 'de' else self.d2
            subprocess.check_call(
                ["cp %s/%s_%sGenerated %s/%s_%sGenerated.orig" % (directory, de, prefix,
                    directory, de, prefix)], shell=True)
            subprocess.check_call(
                ["sed -i -r 's/ @(.*?)@ //g' %s/%s_%sGenerated" % (directory, de, prefix)], shell=True)
            subprocess.check_call(
                ["sed -i -r 's/ @(.*?)@ //g' %s/%s_%s_reference.*" % (directory, de, prefix)], shell=True)

        mmeteor1, mbleu1, mter1 = self.multeval_sub(directory, prefix, self.d1, self.args.meteor_lang1, val)
        mmeteor2, mbleu2, mter2 = self.multeval_sub(directory, prefix, self.d2, self.args.meteor_lang2, val)

        return mmeteor1, mbleu1, mter1, mmeteor2, mbleu2, mter2

    def multeval_sub(self, directory, prefix, dataset, lang, val=True):
        '''
        Subprocess call for multeval
        '''
        with cd(MULTEVAL_DIR):
            subprocess.check_call(
                ['./multeval.sh eval --refs ../%s/%s_%s_reference.* \
                 --hyps-baseline ../%s/%s_%sGenerated \
                 --meteor.language %s \
                 --threads 4 \
                 2> %s-multevaloutput_%s 1> %s-multevaloutput_%s'
                % (directory, dataset, prefix, directory, dataset, prefix,
                    lang, self.args.run_string, dataset,
                    self.args.run_string, dataset)], shell=True)
            handle = open("%s-multevaloutput_%s" % (self.args.run_string, dataset))
            multdata = handle.readlines()
            handle.close()
            for line in multdata:
              if line.startswith("RESULT: baseline: BLEU: AVG:"):
                mbleu = line.split(":")[4]
                mbleu = mbleu.replace("\n","")
                mbleu = mbleu.strip()
                lr = mbleu.split(".")
                mbleu = float(lr[0]+"."+lr[1][0:2])
              if line.startswith("RESULT: baseline: METEOR: AVG:"):
                mmeteor = line.split(":")[4]
                mmeteor = mmeteor.replace("\n","")
                mmeteor = mmeteor.strip()
                lr = mmeteor.split(".")
                mmeteor = float(lr[0]+"."+lr[1][0:2])
              if line.startswith("RESULT: baseline: TER: AVG:"):
                mter = line.split(":")[4]
                mter = mter.replace("\n","")
                mter = mter.strip()
                lr = mter.split(".")
                mter = float(lr[0]+"."+lr[1][0:2])

            logger.info("%s: Meteor = %.2f | BLEU = %.2f | TER = %.2f",
			dataset, mmeteor, mbleu, mter)

            return mmeteor, mbleu, mter

    def create_checkpoint_directory(self, savetime, train_end = False):
        '''
        We will create one directory to store all of the epochs data inside.
        The name is based on the run_string (if provided) or the current time.
        '''

        prefix = self.args.run_string if self.args.run_string != "" else ""
        if train_end:
            number = "train_end"
        else:
            number = "%03d" % (len(self.val_metric1) + 1)
        filepath = "checkpoints/%s/%s-%s" % ((prefix, number, savetime))
        try:
            os.mkdir("checkpoints/%s/" % (prefix))
            shutil.copyfile("main.py", "checkpoints/%s/main.py" % prefix)
            shutil.copyfile("models.py", "checkpoints/%s/models.py" % prefix)
        except OSError:
            pass  # directory already exists
        try:
            os.mkdir(filepath)
        except OSError:
            pass  # directory already exists
        logger.info("\nIn %s ...",filepath)
        return filepath

    def save_run_arguments(self, filepath):
        '''
        Save the command-line arguments, along with the method defaults,
        used to parameterise this run.
        '''
        handle = open("%s/argparse.args" % filepath, "w")
        for arg, value in self.args.__dict__.iteritems():
            handle.write("%s: %s\n" % (arg, str(value)))
        handle.close()

    def checkpoint_parameters(self, epoch, logs, filepath, cur_val_metric1, cur_val_metric2,
                              bleu1, bleu2, cur_val_loss1=0., cur_val_loss2=0.):
        '''
        We checkpoint the model parameters based on either PPLX reduction or
        metric score increase in the validation data. This is driven by the
        user-specified argument self.args.stopping_loss.

	TODO: this doesn't yet support early stopping based on TER
        '''

        weights_path = "%s/weights.hdf5" % filepath

        self.val_loss1.append(cur_val_loss1)
        self.val_loss2.append(cur_val_loss2)
        if cur_val_loss1 < self.best_val_loss1:
            self.best_val_loss1 = cur_val_loss1
        if cur_val_loss2 < self.best_val_loss2:
            self.best_val_loss2 = cur_val_loss2

        # save the weights anyway for debug purposes
        self.model.save_weights(weights_path, overwrite=True)

        # update the best values, if applicable
        self.val_metric1.append(cur_val_metric1)
        self.val_metric2.append(cur_val_metric2)
        if cur_val_metric1 > self.best_val_metric1:
            self.best_val_metric1 = cur_val_metric1
        if cur_val_metric2 > self.best_val_metric2:
            self.best_val_metric2 = cur_val_metric2

        # update the best values, if applicable
        self.val_bl_metric1.append(bleu1)
        self.val_bl_metric2.append(bleu2)
        if bleu1 > self.best_val_bl_metric1:
            self.best_val_bl_metric1 = bleu1
        if bleu2 > self.best_val_bl_metric2:
            self.best_val_bl_metric2 = bleu2



        optimiser_params = open("%s/optimiser_params" % filepath, "w")
        for key, value in self.model.optimizer.get_config().items():
            optimiser_params.write("%s: %s\n" % (key, value))
        optimiser_params.close()

    def reset_text_arrays(self, text_arrays, fixed_words=1):
        """ Reset the values in the text data structure to zero so we cannot
        accidentally pass them into the model """
        reset_arrays = deepcopy(text_arrays)
        reset_arrays[:,fixed_words:, :] = 0
        return reset_arrays

    def generate_sentences(self, filepath, val=True):
        """
        Generates descriptions of images for --generation_timesteps
        iterations through the LSTM. Each input description is clipped to
        the first <BOS> token, or, if --generate_from_N_words is set, to the
        first N following words (N + 1 BOS token).
        This process can be additionally conditioned
        on source language hidden representations, if provided by the
        --source_vectors parameter.
        The output is clipped to the first EOS generated, if it exists.

        TODO: duplicated method with generate.py
        """
        prefix = "val" if val else "test"
        logger.info("Generating %s descriptions", prefix)
        start_gen = self.args.generate_from_N_words + 1  # include BOS
        handle1 = codecs.open("%s/%s_%sGenerated" % (filepath, self.d1, prefix),
                             "w", 'utf-8')
        handle2 = codecs.open("%s/%s_%sGenerated" % (filepath, self.d2, prefix),
                             "w", 'utf-8')


        val_generator = self.data_generator.generation_generator(prefix,
                                                                 in_callbacks=True)
        seen = 0
        for data in val_generator:
            inputs = data[0]
            text1 = deepcopy(inputs['text1'])
            text2 = deepcopy(inputs['text2'])
            # Append the first start_gen words to the complete_sentences list
            # for each instance in the batch.
            complete_sentences1 = [[] for _ in range(text1.shape[0])]
            complete_sentences2 = [[] for _ in range(text2.shape[0])]
            for t in range(start_gen):  # minimum 1
                for i in range(text1.shape[0]):
                    w = np.argmax(text1[i, t])
                    complete_sentences1[i].append(self.index2word_d1[w])
                for i in range(text2.shape[0]):
                    w = np.argmax(text2[i, t])
                    complete_sentences2[i].append(self.index2word_d2[w])

            del inputs['text1']
            del inputs['text2']
            text1 = self.reset_text_arrays(text1, start_gen)
            text2 = self.reset_text_arrays(text2, start_gen)
            Y_target = data[1]
            #Y_target1 = data[1]
            #Y_target2 = data[1]
            inputs['text1'] = text1
            inputs['text2'] = text2

            for t in range(start_gen, self.args.generation_timesteps):
                #logger.debug("Input token: %s" % self.index2word_d1[np.argmax(inputs['text1'][0,t-1])])
                #logger.debug("Input token: %s" % self.index2word_d2[np.argmax(inputs['text2'][0,t-1])])
                preds1, preds2 = self.model.predict(inputs, verbose=0)
                #logger.info("shape of predictions: %s" % preds.shape)

                # Look at the last indices for the words.
                #next_word_indices = np.argmax(preds['output'][:, t-1], axis=1)
                next_word_indices1 = np.argmax(preds1[:, t-1], axis=1)
                next_word_indices2 = np.argmax(preds2[:, t-1], axis=1)
                #logger.debug("Predicted token: %s" % self.index2word[next_word_indices[0]])
                # update array[0]/sentence-so-far with generated words.
                for i in range(len(next_word_indices1)):
                    inputs['text1'][i, t, next_word_indices1[i]] = 1.
                for i in range(len(next_word_indices2)):
                    inputs['text2'][i, t, next_word_indices2[i]] = 1.

                next_words1 = [self.index2word_d1[x] for x in next_word_indices1]
                next_words2 = [self.index2word_d2[x] for x in next_word_indices2]
                for i in range(len(next_words1)):
                    complete_sentences1[i].append(next_words1[i])
                for i in range(len(next_words2)):
                    complete_sentences2[i].append(next_words2[i])


            sys.stdout.flush()
            # print/extract each sentence until it hits the first end-of-string token
            for s in complete_sentences1:
                decoded_str = ' '.join([x for x
                                        in itertools.takewhile(
                                            lambda n: n != "<E>", s[1:])])
                handle1.write(decoded_str + "\n")

            for s in complete_sentences2:
                decoded_str = ' '.join([x for x
                                        in itertools.takewhile(
                                            lambda n: n != "<E>", s[1:])])
                handle2.write(decoded_str + "\n")


            seen += text1.shape[0]
            if seen >= self.data_generator.split_sizes['val']:
                # Hacky way to break out of the generator
                break
        handle1.close()
        handle2.close()
